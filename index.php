<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" integrity="sha512-NmLkDIU1C/C88wi324HBc+S2kLhi08PN5GDeUVVVC/BVt/9Izdsc9SVeVfA1UZbY3sHUlDSyRXhCzHfr6hmPPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href='/css/style.css'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js""></script>

    <title>Document</title>
</head>
<body>

<?php


$link = mysqli_connect('localhost', 'root', 'root', 'first_store'); // Соединяемся с базой

// Ругаемся, если соединение установить не удалось
if (!$link) {
    echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
    exit;
}
include 'data/createTable_fileRozetka.php'; // создает таблицы

//розекта
//$file = simplexml_load_file('data/price_rozetka_1.xml',null,LIBXML_NOCDATA);



if (!empty($_POST['btnPrepare'])){
    $file = simplexml_load_file('data/fileLocal.xml');
}

// добавляет все товары с контрольной суммой(rozetka)
//function addAllProduct($link,$obj){
//    foreach ($obj as $key => $item) {
//        $arrSum = json_encode($item);
//        $controlSum = crc32($arrSum);
//                mysqli_query($link, "INSERT INTO `product`
//        (`id_product`,`control_sum`, `available`, `price`, `price_old`, `price_promo`, `stock_quantity`, `currencyId`, `categoryId`, `name`, `name_ua`, `article`, `vendor`, `description`, `description_ua`)
//        VALUES ('{$item['id']}','$controlSum','{$item['available']}','$item->price','$item->price_old','$item->price_promo','$item->stock_quantity','$item->currencyId','$item->categoryId','$item->name','$item->name_ua','$item->article','$item->vendor','$item->description','$item->description_ua')");
//
//    }
//}
//addAllProduct($link,$file->shop->offers->offer);

// добавляет все параметры с контрольной суммой(rozetka)
//function addAllParam($link,$file){
//    foreach ($file as $item) {
//        foreach ($item->param as $item2) {
//            $arrToStr = json_encode($item2);
//            $controlSum = crc32($arrToStr);
//            $value = trim($item2);
//            mysqli_query($link, "INSERT INTO `param`(`control_sum`, `id_param`, `id_product`, `id_value`, `value_name`, `value`) VALUES ('$controlSum','{$item2['paramid']}','{$item['id']}','{$item2['valueid']}','{$item2['name']}','$value')");
//        }
//    }
//}
////addAllParam($link,$file->shop->offers->offer);

if (!empty($file)){
//    addAllProduct($link,$file->shop->offers->offer);
//    addAllParam($link,$file->shop->offers->offer);
//    addUrlImg($link,$file->shop->offers->offer);
}

//addCategory($link,$file->shop->categories->category);
//добавляет категории
function addCategory($link,$file){
    foreach ($file as $item) {
        mysqli_query($link,"INSERT INTO `category`(`id_category`, `category_name`) VALUES ('{$item['id']}','$item')");
    }
}



// добавляет все товары в бд с контрольной суммой
function addAllProduct($link,$obj){
    foreach ($obj as $key => $item) {
        $arrSum = json_encode($item);
        $controlSum = crc32($arrSum);
        mysqli_query($link, "INSERT INTO `product`
        (`id_product`,`control_sum`, `available`, `price`, `currencyId`, `categoryId`,`pickup`,`delivery`, `name`, `vendor`,`vendorCode`,`country_of_origin`, `description`)
        VALUES ('{$item['id']}','$controlSum','{$item['available']}','$item->price','$item->currencyId','$item->categoryId','$item->pickup','$item->delivery','$item->name','$item->vendor','$item->vendorCode','$item->country_of_origin','$item->description')");

    }
}

// добавляет все параметры в бд с контрольной суммой
function addAllParam($link,$file){
    foreach ($file as $item) {
        foreach ($item->param as $item2) {
            $arrToStr = json_encode($item2);
            $controlSum = crc32($arrToStr);
            $value = trim($item2);
            mysqli_query($link, "INSERT INTO `param`
    ( `control_sum`, `id_product`, `value_name`, `value`, `unit`) 
VALUES ('$controlSum','{$item['id']}','{$item2['name']}','$value','{$item2['unit']}')");
        }
    }
}

// добавляет url контрольную сумму картинок
function addUrlImg($link,$file){
    foreach ($file as $item) {
        foreach ($item->picture as $item2) {
            $arrSum = json_encode($item);
            $controlSum = crc32($arrSum);
            $id = (string) $item['id'];
            mysqli_query($link, "INSERT INTO `picture`(`control_sum`, `id_product`, `url`) VALUES ('$controlSum','$id','$item2')");
        }

    }
}

// добавляет ид продукта в качестве ключа массива ,служит для подготовки и вычеслению контр суммы
function addProductId($file){
    foreach ($file as $item) {
        $id = (string) $item['id'];  // нужно привести к строке, чтобы получить значение, а не экземпляр
        $obj[$id] = $item ;
    }
    return $obj;
}

// добавляет ид парметра в качестве ключа массива ,служит для подготовки к вычеслению контр суммы
$fileForParam = addParamId($file->shop->offers->offer);
function addParamId($file){
    foreach ($file as $item) {
        foreach ($item->param as $item2) {
            $idProd = (string) $item['id'];
            $id = (string) $item2['id'];
            $sum[$idProd][] = $item2;
        }
    }
    return $sum;
}

// считает контрольную сумму обьекта , передавать нужно подготовленый файл с ид ключами
function controlSum($file){
    if (!empty($file)){
        //обьект в строку
        foreach ($file as $key => $item) {
            $arrSum[$key] = json_encode($item); // в строку
        }
//        return $arrSum;
        // контрольная сумма обьекта
        foreach ($arrSum  as $key => $item2) {
            $crc[$key] = crc32($item2);
        }
        return $crc;
    }
}

// контрольная сумма для параметров
function controlSumForParam($file){
    if (!empty($file)){
        //обьект в строку
        foreach ($file as $key => $item) {
            foreach ($item as $key2 => $item2) {
                $arrSum[$key][$key2] = json_encode($item2); // в строку
            }
        }
        // контрольная сумма обьекта
        foreach ($arrSum  as $key => $item) {
            foreach ($item as $key2 => $item2) {
                $crc[$key][$key2] = crc32($item2);
            }
        }
        return $crc;
    }
}

function getContrSumProduct($link){
    $get = mysqli_query($link,"SELECT id_product , control_sum FROM `product` ");
    foreach ($get as $item) {
        $product[$item['id_product']] = $item['control_sum'];
    }
    return $product;
}
function getContrSumParam($link){
    $get = mysqli_query($link,"SELECT id , id_product , control_sum  FROM `param` ");
    foreach ($get as $item) {
        $param[$item['id_product']][$item['id']] = $item['control_sum'];
    }
    return $param;
}

function diffProduct($link,$file){
    $product_DB = getContrSumProduct($link); //контрольная сума с базы
    $product_File = controlSum($file); // контрольная сума файла

    $diffProduct = array_diff($product_File,$product_DB); // ищет не совпадающее значение
//    $diffProduct2 = array_diff($product_DB,$product_File);
    if (!empty($diffProduct) || !empty($diffProduct2) ){

        foreach ($diffProduct as $key => $item) { // ключи стают значениями
            $change[$key] = $key;
        }
        foreach ($file as $key => $item) { // по ключу берет элемент для обновления
            if (in_array($key,$change)){
                $productForUpdate[$key] = $item;
            }
        }
    }
    return $productForUpdate;
}


if (!empty($file)){
    $fileForProduct = addProductId($file->shop->offers->offer);
    addNewProduct($link,$file->shop->offers->offer);
    updateDBProduct($link,$fileForProduct);
}

function addNewProduct($link,$file){
    $product_DB = getContrSumProduct($link);
    $product_File = addProductId($file);
    $countDB = count($product_DB);
    $countFile = count($product_File);

    if ($countDB < $countFile || $countDB > $countFile){

        $diff = array_diff(controlSum($product_File),$product_DB); // новый товар в файле
        $diffNot = array_diff($product_DB,controlSum($product_File)); // тот товар которого нету в файле
        if (!empty($diff)){
            foreach ($diff as $key => $item) { // ключи стают значениями
                $change[$key] = $key;
            }
            foreach ($product_File as $key => $item) { // по ключу ищет элемент для обновления
                if (in_array($key,$change)){
                    $productForUpdate[$key] = $item;
                }
            }
            addAllProduct($link,$productForUpdate);  // sql добавляет новый товар в базу
            addAllParam($link,$productForUpdate); // sql добавляет параметры к новому товару
        }else{
            // если товар есть в базе но нету в файле (не забыть прописать вариант когда есть $diffNot и $diff)
        }
    }
}
// аттрибуты (подключени к БД , готовый файл с ключами в виде ид товара/параметра)
function updateDBProduct($link,$file){
    $product_DB = getContrSumProduct($link); //контрольная сума с базы
    $product_File = controlSum($file); // контрольная сума файла

    $diffProduct = array_diff($product_File,$product_DB); // ищет не совпадающее значение
    $diffProduct2 = array_diff($product_DB,$product_File);

    if (!empty($diffProduct) || !empty($diffProduct2) ){

        foreach ($diffProduct as $key => $item) { // ключи стают значениями
            $change[$key] = $key;
        }
        foreach ($file as $key => $item) { // по ключу берет элемент для обновления
            if (in_array($key,$change)){
                $productForUpdate[$key] = $item;
            }
        }

        foreach ($productForUpdate as $key => $item){
            $arrSum = json_encode($item);
            $controlSum = crc32($arrSum);
            mysqli_query($link,"UPDATE `product` SET `id_product`='{$item['id']}',`control_sum`='$controlSum',`available`='{$item['available']}',`price`='$item->price',
                     `currencyId`='$item->currencyId',`categoryId`='$item->categoryId',`pickup`='$item->pickup',`delivery`='$item->delivery',`name`='$item->name',`vendor`='$item->vendor',
                     `vendorCode`='$item->vendorCode',`country_of_origin`='$item->country_of_origin',`description`='$item->description' WHERE product.id_product = '$key'");
        }
    }
}


if (!empty($file) && !empty(searchNewParams($link,$fileForParam))){
     setNewParam($link,searchNewParams($link,$fileForParam));
}
if (!empty($file)){
    $diffParamForUpdate = diffParam($link,$fileForParam);
    if (!empty($diffParamForUpdate)){
    updateDBParam($link,$diffParamForUpdate);
    }
}

// ищет новые параметры если такие есть возвращает полный набор параметров товара
function searchNewParams($link,$file){
    $param_DB = getContrSumParam($link); //контрольная сума с базы
    $product_File = controlSumForParam($file); // контрольная сума файла

    foreach ($param_DB as $item) {
        foreach ($item as $key => $item2) {
            $resultDB[$key] = $item2;
        }
    }

// собираю значения в 1 массив из файла
    foreach ($product_File as $item) {
        foreach ($item as $item2) {
            $resultFile[] = $item2;
        }
    }

// смотрю по длине 2 массива если есть новый - ищет его
    if (count($resultDB) !== count($resultFile)){
        if (!empty(array_diff($resultDB,$resultFile))){
            $diffParam = array_diff($resultDB,$resultFile);
        }else{
            $diffParam = array_diff($resultFile,$resultDB);
        }
    }

//по контрольной сумме ищет новый параметр
    foreach ($diffParam as $item) {
        foreach ($product_File as $key => $item2) {
            if (in_array($item,$item2)){
                $res[$key] = $key;
            }
        }
    }
    // удалит если в файле удален параметр
    if (empty($res)){
        foreach ($diffParam as $key => $item) {
            mysqli_query($link,"DELETE FROM `param` WHERE param.id = '$key'");
        }
    }else{
        foreach ($file as $key => $item) { // по ключу берет элемент для обновления
            if (in_array($key,$res)){
                $additionParam[$key] = $item;
            }
        }
        return $additionParam;
    }
}

// записывает новые параметры в бд (чистит старые записывает новый)
function setNewParam($link,$newParams){

    foreach ($newParams as $key => $item){
        mysqli_query($link,"DELETE FROM `param` WHERE param.id_product = '$key'");
    }

    foreach ($newParams as $key => $item) {
        foreach ($item as $item2) {
            $arrToStr = json_encode($item2);
            $controlSum = crc32($arrToStr);
            $value = trim($item2);
            mysqli_query($link, "INSERT INTO `param`
    ( `control_sum`, `id_product`, `value_name`, `value`, `unit`)
VALUES ('$controlSum','$key','{$item2['name']}','$value','{$item2['unit']}')");
        }
    }
}

// ищет разницу в параметрах базы и файла
function diffParam($link,$file){
    $param_DB = getContrSumParam($link); //контрольная сума с базы
    $product_File = controlSumForParam($file); // контрольная сума файла


// собираю значения в 1 массив из БД
    foreach ($param_DB as $item) {
        foreach ($item as $key => $item2) {
            $resultDB[$key] = $item2;
        }
    }

// собираю значения в 1 массив из файла
    foreach ($product_File as $item) {
        foreach ($item as $item2) {
            $resultFile[] = $item2;
        }
    }

// поиск разници
    $diffParam = array_diff($resultFile,$resultDB);

// присвоение значениям ключей (ид продукта)
    foreach ($product_File as $key => $item) {
        foreach ($diffParam as $item2) {
            if (in_array($item2,$item)){
                $result[$key][] = $item2;
            }
        }
    }

    foreach ($result as $key => $item) { // ключи стают значениями
        $change[$key] = $key;
    }

    foreach ($file as $key => $item) { // по ключу берет элемент для обновления
        if (in_array($key,$change)){
            $productForUpdate[$key] = $item;
        }
    }

    return $productForUpdate;
}

// обновляет
function updateDBParam($link,$diffParamForUpdate){
    $productForUpdate = $diffParamForUpdate;

    foreach ($productForUpdate as $key => $item2) {
        foreach ($item2 as $item) {
            $arrSum = json_encode($item);
            $controlSum = crc32($arrSum);
            $value = trim($item);
            mysqli_query($link,"UPDATE `param` SET
                   `control_sum`='$controlSum',`value_name`='{$item['name']}',`value`='$value',`unit`='{$item['unit']}' WHERE param.id_product = '$key' AND param.value_name = '{$item['name']}'");
        }
    }
}

// все товары
function getProduct($link){
    $get = mysqli_query($link,"SELECT id_product, `name` , price , description FROM `product` LIMIT 3 OFFSET 1");
    foreach ($get as $item) {
        $product[] = $item;
    }
    return $product;
}


// категории с количеством товаров в ней
function getCategory($link){
//    $get = mysqli_query($link,"SELECT COUNT(categoryId), category_name,id_category FROM product INNER JOIN category ON category.id_category = product.categoryId GROUP BY categoryId;");
    $get = mysqli_query($link,"SELECT category_name,id_category FROM category");
    foreach ($get as $item) {
        $categoryName[] = $item;
    }
    return $categoryName;
}

// товар по выбраной категории
function getProductByCategory($link){
    $idCategory = $_GET['category'];
    $get = mysqli_query($link,"SELECT id_product , name , price ,categoryId, description FROM `product` WHERE product.categoryid = '$idCategory'");
    foreach ($get as $item){
        $product[] = $item;
    }
    return $product;
}

//товар по Ид для детальной страницы
function getProductByID($link){
    $idProduct = $_GET['product'];
    $get = mysqli_query($link,"SELECT id_product ,available, name , price , description,vendor FROM `product` WHERE product.id_product = '$idProduct'");
    foreach ($get as $item){
        $product[] = $item;
    }
    return $product;
}

//параметры по ид
function getParamByIdProd($link){
    $idProduct = $_GET['product'];
    $get = mysqli_query($link,"SELECT * FROM `param` WHERE param.id_product = '$idProduct'");
    foreach ($get as $item){
        $product[] = $item;
    }
    return $product;
}

function getImgByIdProd($link,$id){
    $get = mysqli_query($link,"SELECT `path` FROM `picture` WHERE `id_product` = '$id'");
    foreach ($get as $item) {
        $res[] = $item['path'];
    }
    return $res;
}


include 'btn.php';
?>
<header class="header">
    <div class="wrapper-logo">
        <a  href="/"><img class="logo" src="img/vecteezy_circle-logo_1191989.png" alt=""></a>
    </div>

    <?php include 'view/burger-menu_view.php'; ?>
</header>
<main>

    <?php
    if (empty($_GET['product'])){
        include 'view/product_view.php';
    }
    ?>

</main>

<?php
if (!empty($_GET['product'])){
    include 'view/productDetails_view.php';
}


?>

<div class="btn-download">
    <button id="ajax-btn-load" class="btn btn-secondary" name="btnDownload" >Загрузка файла XML</button>
</div>

<br/>

<div class="btn-prepare-file">
    <form action="" method="post">
        <input class="btn btn-primary" type="submit" name="btnPrepare" value="Подготовка файла">
    </form>
</div>

<div class="wrapper-counter">
    <h1 id="counter"></h1>
</div>


<br/>
<div class="" id="wrapper1">
    <button type="button" class="btn btn-primary" id="ajax-with-simple-dialog">Загрузка изображений</button>
    <br/>
</div>


<footer class="footer"></footer>
<script src="js/ajax.js" ></script>
<script src="js/script.js"></script>
</body>
</html>