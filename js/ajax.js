

function loadImg(){
    let wrLoad = document.getElementById('progress-bar1');
    if (wrLoad === null){
        var statuss1 = 'false';
    }else {
        statuss1 = 'true';
    }

    $.ajax({
        type: "POST", //we are using POST method to submit the data to the server side
        url: 'btn.php', // get the route value
        data: {accept: 'true', progressBar: statuss1},
        beforeSend: function () {//We add this before send to disable the button once we submit it so that we prevent the multiple click

            if (wrLoad === null){
                let wrLoad = document.querySelector('body');
                wrLoad.insertAdjacentHTML(
                    "beforeend",
                    `<div id="progress-bar1">
                                <div class="wrap-prog-line">
                                    <div id="progress_line"></div>
                                </div>
                                <div id="percent-progress">0%</div>
                           </div>`
                )
            }
        },
        success: function (response) { //как только запрос будет успешно обработан на стороне сервера, он вернет результат здесь

            let objCountImg = JSON.parse(response);

            if (objCountImg.allImg !== objCountImg.currentImg){ // повторный вызов функции елси не все изображения загрузились
                loadImg();
                let onePercent = objCountImg.allImg/100;
                let currentPercent = objCountImg.currentImg/onePercent;

                let line = document.getElementById('progress_line');
                line.style.width = currentPercent + '%';
                let percentP = Math.ceil(currentPercent);
                let domPercentProgress = document.getElementById('percent-progress');
                domPercentProgress.innerHTML = percentP + '%';
            }else {
                let line = document.getElementById('progress_line');
                line.style.width = 100 + '%';
                let domPercentProgress = document.getElementById('percent-progress');
                domPercentProgress.innerHTML = 100 + '%';


                let btnClose = document.getElementById('progress-bar1');
                btnClose.insertAdjacentHTML(
                    "beforeend",
                    `<button id="btn-close">Закрыть</button>`
                )
                btnClose.insertAdjacentHTML(
                    "afterbegin",
                    `<p id="complete">Готово!</p>`
                )
                $('#btn-close').on('click',function (){
                    btnClose.remove();
                })
            }
        },
        complete: function() {

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

$(document).ready(function (){
    $('#ajax-with-simple-dialog').on('click',loadImg);
})










// $(document).ready(function() {
//     $('#ajax-with-simple-dialog').on('click', function() {
//
//         $.ajax({
//             type: "POST", //we are using POST method to submit the data to the server side
//             url: 'btn.php', // get the route value
//             data: {accept: 'true'},
//             beforeSend: function () {//We add this before send to disable the button once we submit it so that we prevent the multiple click
//                 let wrLoad = document.getElementById('wrapper1');
//                 wrLoad.insertAdjacentHTML(
//                     "beforeend",
//                     `<div id="wrapper2" class="spinner-border" role="status">
//                                  <span class="visually-hidden">Loading...</span>
//                           </div>`
//                 )
//             },
//             success: function (response) {//once the request successfully process to the server side it will return result here
//                 // console.log(response);
//             },
//             complete: function() {
//                 let wrLoad2 = document.getElementById('wrapper2');
//                 wrLoad2.remove();
//                 alert('Изображения успешно добавлены')
//             },
//             error: function (XMLHttpRequest, textStatus, errorThrown) {
//             }
//         });
//     });
// });



// слушатель на загрузку файла xml
$('#ajax-btn-load').on('click',function (){

    $.ajax({
        type: "POST", //we are using POST method to submit the data to the server side
        url: 'btn.php', // get the route value
        data: {loadFile:'true'},
        beforeSend: function () {//We add this before send to disable the button once we submit it so that we prevent the multiple click
            let wrLoad = document.getElementById('wrapper1');
            wrLoad.insertAdjacentHTML(
                "beforeend",
                `<div id="wrapper2" class="spinner-border" role="status">
                                 <span class="visually-hidden">Loading...</span>
                          </div>`
            )
        },
        success: function (response) {//once the request successfully process to the server side it will return result here
            if (response === 'false'){
                let wrLoad = document.getElementById('wrapper1');
                wrLoad.insertAdjacentHTML(
                    "beforeend",
                    `<div class="er">
                               <h2 class="er-text">Ошибка файла! Загрузите еще раз</h2>
                            </div>`
                )
                let wrLoad2 = document.getElementById('wrapper2');
                wrLoad2.remove();
            }else {
                let wrLoad2 = document.getElementById('wrapper2');
                wrLoad2.remove();
                alert('Файл XML добавлен')
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
})


