<?php
function createTable($link){
    mysqli_query($link,
        "CREATE TABLE `first_store`.`product` 
(
`id_product` INT(30) NOT NULL , 
`control_sum` VARCHAR(100) NOT NULL , 
`available` VARCHAR(30) NOT NULL , 
`price` INT(20) NOT NULL , 
`currencyId` VARCHAR(20) NOT NULL , 
`categoryId` INT(20) NOT NULL ,  
`pickup` VARCHAR(100) NOT NULL , 
`delivery` VARCHAR(100) NOT NULL , 
`name` VARCHAR(100) NOT NULL , 
`vendor` VARCHAR(20) NOT NULL , 
`vendorCode` VARCHAR(100) NOT NULL , 
`country_of_origin` VARCHAR(100) NOT NULL , 
`description` VARCHAR(2000) NOT NULL , 
PRIMARY KEY (`id_product`)) 
    ENGINE = InnoDB;");

    //промежуточная таблица для картинок
    mysqli_query($link, "CREATE TABLE `first_store`.`picture_product` 
( `id` INT NOT NULL AUTO_INCREMENT , 
`id_product` INT NOT NULL , 
`id_picture` INT NOT NULL , PRIMARY KEY (`id`)) 
    ENGINE = InnoDB;");

    // таблица с изображениями
    mysqli_query($link,"CREATE TABLE `first_store`.`picture` 
( `id_picture` INT NOT NULL AUTO_INCREMENT , 
`name_picture` VARCHAR(1000) NOT NULL , 
PRIMARY KEY (`id_picture`)) ENGINE = InnoDB");

    // парамметры товара
    mysqli_query($link,"CREATE TABLE `first_store`.`param` 
( `id` INT(10) NOT NULL AUTO_INCREMENT ,
`control_sum` VARCHAR(100) NOT NULL ,
`id_product` INT(10) NOT NULL ,
`value_name` VARCHAR(50) NOT NULL , 
`value` VARCHAR(1000) NOT NULL , 
`unit` VARCHAR(100) NOT NULL , 
PRIMARY KEY (`id`)) ENGINE = InnoDB;");

//    // промежуточная товар_значение
//    mysqli_query($link,"CREATE TABLE `first_store`.`product_param`
//( `id` INT(10) NOT NULL AUTO_INCREMENT ,
//`id_product` INT(10) NOT NULL ,
//`id_param` INT(10) NOT NULL ,
//PRIMARY KEY (`id`)) ENGINE = InnoDB;");

//    // связи для промежуточной таблици
//    mysqli_query($link,"
//ALTER TABLE `first_store`.`product_param` ADD FOREIGN KEY (`id_param`) REFERENCES `first_store`.`param`(`id_param`) ON DELETE CASCADE ON UPDATE CASCADE;
//ALTER TABLE `first_store`.`product_param` ADD FOREIGN KEY (`id_product`) REFERENCES `first_store`.`product`(`id_product`) ON DELETE CASCADE ON UPDATE CASCADE;");
}