<div class="burger-menu">
    <button class="burger-menu_button" onclick="showCategory()">Каталог</button>
    <nav class="burger-menu_nav">
        <?php foreach (getCategory($link) as $item) :?>
            <a href="<?php echo "?category=".$item['id_category']?>" class="burger-menu_link"><?php echo $item['category_name']?> (<?php echo $item['COUNT(categoryId)']?>)</a>
        <?php endforeach;?>
    </nav>
    <div class="overflow"></div>
</div>