<?php //echo 'pnx' ?>
<div class="container-product-details">
    <?php foreach (getProductByID($link) as $item) : ?>
        <div class="wrapper-product-details">
            <div class="product-title-code">
                <p class="product-title"><?php echo $item['name']?></p>
                <p class="product-code">Код: <?php echo $item['id_product']?></p>
            </div>

            <div class="img-price-available">
                <div class="wrapper-img-product">
                    <?php foreach (getImgByIdProd($link,$item['id_product']) as $item2) : ?>
                        <img src="<?php echo $item2 ?>" alt="img" class="img-product">
                    <?php endforeach; ?>
                </div>

                <div class="price-available">
                    <div class="product-available <?= $item['available'] == 'false' ? 'false' : '' ?>">
                        <p class="available-title"><?= $item['available'] == 'true' ? 'Есть в наличии': 'Нет в наличии'; ?></p>
                    </div>
                    <p class="product-prices"><?php echo $item['price']?> грн</p>

                    <div class="wrapper-vendor">
                        <h1 class="title-vendor">Продавец:</h1>
                        <p class="vendor"><?php echo $item['vendor']?></p>
                    </div>
                </div>

            </div>
            <h1 class="param-title">Описание:</h1>
            <p class="description"><?php echo $item['description']?></p>
        </div>
        <div class="product-param">
            <h1 class="param-title">Характеристики:</h1>
            <?php foreach (getParamByIdProd($link) as $item2) : ?>
                <div class="wrapper-param">
                    <p class="name-param"><?php echo $item2['value_name']?>:</p>

                    <p class="value-param"><?php echo $item2['value']?></p>
                </div>

            <?php endforeach;?>

        </div>
    <?php endforeach;?>
</div>

