<div class="container-product">
    <?php if (empty($_GET['category'])): ?>
        <?php foreach (getProduct($link) as $item) :?>
            <div class="product">
                <img src="<?php echo getImgByIdProd($link,$item['id_product'])[0]?>"  alt="img" class="img-product">
                <a href="<?php echo "?product=".$item['id_product']?>" class="name-product"><?php echo $item['name']?></a>
                <p class="price-product"><?php echo $item['price']?> грн</p>
                <!--            <p class="description-product">--><?php //echo $item['description']?><!--</p>-->
            </div>
        <?php endforeach;?>
    <?php endif; ?>


    <?php if (!empty($_GET['category'])): ?>
        <?php foreach (getProductByCategory($link) as $item) :?>
            <div class="product">
                <img src="/<?php echo getImgByIdProd($link,$item['id_product'])[0]?>"  alt="img" class="img-product">
                <a href="<?php echo "?product=".$item['id_product']?>" class="name-product"><?php echo $item['name']?></a>
                <p class="price-product"><?php echo $item['price']?> грн</p>
                <!--            <p class="description-product">--><?php //echo $item['description']?><!--</p>-->
            </div>
        <?php endforeach;?>
    <?php endif; ?>
</div>